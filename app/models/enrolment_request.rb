class EnrolmentRequest < ApplicationRecord
  Learner = Struct.new(:first_name, :last_name, :email)
  Degree = Struct.new(:title, :code)

  def new?
    status == 'new'
  end

  def accept!
    set_status('accepted')
  end

  def submit!
    set_status('submitted')
  end

  def degree
    @degree ||=  begin
      api_token = ApiToken.new.create
      connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
      connection.authorization(:Bearer, api_token)

      response = connection.get('/partners/degrees/' + degree_uuid)
      json = JSON.parse(response.body)

      Degree.new(json['title'], json['code'])
    end
  end

  def learner
    @learner ||= begin
      api_token = ApiToken.new.create
      connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
      connection.authorization(:Bearer, api_token)

      response = connection.get('/partners/learners/' + learner_uuid)
      json = JSON.parse(response.body)

      Learner.new(json['first_name'], json['last_name'], json['email'])
    end
  end

  private

  def set_status(status)
    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.patch('/partners/enrolment_requests/' + uuid, status: status)

    update!(status: status)
  end
end
