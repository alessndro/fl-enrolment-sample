class ApiToken
  def create
    JWT.encode(
      {
        iss: Rails.configuration.consumer_key,
        iat: Time.now.to_i
      },
      Rails.configuration.shared_secret,
      'HS256'
    )
  end
end
