class Admin::EnrolmentRequestsController < ApplicationController
  before_action :find_enrolment_request, only: [:accept, :confirm, :show]

  def index
    @enrolment_requests = EnrolmentRequest.all
  end

  def accept
    @enrolment_request.accept!
    flash[:notice] = 'Enrolment request successfully accepted!'
    redirect_to admin_enrolment_requests_path
  end

  def confirm
    organisation_membership_uuid = create_organisation_membership

    degree_enrolment_uuid = confirm_enrolment_request(organisation_membership_uuid)

    program_run_uuid = find_program_run_uuid

    create_program_enrolments(organisation_membership_uuid, program_run_uuid, degree_enrolment_uuid)

    @enrolment_request.update!(status: 'confirmed')

    flash[:notice] = 'Enrolment request successfully confirmed!'
    redirect_to admin_enrolment_requests_path
  end

  private

  def find_enrolment_request
    @enrolment_request = EnrolmentRequest.find(params[:id])
  end

  def create_organisation_membership
    external_learner_id = params[:external_learner_id]

    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.post('/partners/organisation_memberships', {
      external_learner_id: external_learner_id,
      learner: {
        uuid: learner_uuid
      }
    })

    json = JSON.parse(response.body)
    json['uuid']
  end

  def confirm_enrolment_request(organisation_membership_uuid)
    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.patch('/partners/enrolment_requests/' + uuid, {
      organisation_membership: {
        uuid: organisation_membership_uuid,
      },
      status: 'confirmed'
    })

    json = JSON.parse(response.body)
    json['degree_enrolment']['uuid']
  end

  def create_program_enrolments(organisation_membership_uuid, program_run_uuid, degree_enrolment_uuid)
    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.post('/partners/program_enrolments', {
      organisation_membership: {
        uuid: organisation_membership_uuid,
      },
      program_run: {
        uuid: program_run_uuid
      },
      degree_enrolment: {
        uuid: degree_enrolment_uuid
      }
    })

    debugger
  end

  def find_program_run_uuid
    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.get('/partners/program_runs/find', program_code: params[:program_code], program_run_code: params[:program_run_code])

    json = JSON.parse(response.body)
    json['uuid']
  end

  def uuid
    @enrolment_request.uuid
  end

  def learner_uuid
    @enrolment_request.learner_uuid
  end

  # attr_reader :enrolment_request

  # delegate :uuid, :learner_uuid, to: :enrolment_request
end
