class EnrolmentRequestsController < ApplicationController
  def new
    @enrolment_request = EnrolmentRequestService.new.find_by_token(params[:token])
  end

  def create
    enrolment_request = EnrolmentRequestService.new.find_by_token(params[:token])
    enrolment_request.accept!
    redirect_to enrolment_request.return_url
  end
end
