require 'faraday'

class EnrolmentRequestService
  def find_by_token(token)
    if enrolment_request = EnrolmentRequest.find_by_token(token)
      enrolment_request
    else
      get_enrolment_request_via_api(token)
    end
  end

  private

  def get_enrolment_request_via_api(enrolment_request_token)
    api_token = ApiToken.new.create
    connection = Faraday.new(url: Rails.configuration.api_base_endpoint)
    connection.authorization(:Bearer, api_token)

    response = connection.post('/partners/enrolment_requests/token_exchange', token: enrolment_request_token)
    fail 'not found' unless response.success?
    json = JSON.parse(response.body)

    EnrolmentRequest.create!(uuid: json['uuid'], token: enrolment_request_token, learner_uuid: json['learner']['uuid'], degree_uuid: json['degree']['uuid'], return_url: json['return_url'])
  end
end
