require 'jwt'

module FutureLearnApi
  module Connection
    BASE_ENDPOINT = 'http://api.local3000.dev'

    private

    def connection
      Faraday.new(url: BASE_ENDPOINT) do |faraday|
        faraday.authorization(:Bearer, api_token)
      end
    end

    def api_token
      api_config = Rails.configuration.x.api_config
      consumer_key = api_config.fetch(:consumer_key)
      shared_secret = api_config.fetch(:shared_secret)

      JWT.encode(
        {
          iss: consumer_key,
          iat: Time.now.to_i
        },
        shared_secret,
        'HS256'
      )
    end
  end
end
