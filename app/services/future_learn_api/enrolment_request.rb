module FutureLearnApi
  class EnrolmentRequest
    include Connection

    def get(uuid)
      response = connection.get(
        '/partners/enrolment_requests/' + uuid
      )
      debugger
      JSON.parse(response.body)
    end

    def post
    end

    def patch
    end

    def token_exchange
      response = connection.post(
        '/partners/enrolment_requests/token_exchange',
        token: enrolment_request_token
      )

      JSON.parse(response.body)
    end
  end
end
